<?php

namespace Database\Factories;

use App\Models\Feed;
use App\Models\TiktokSource;
use Illuminate\Database\Eloquent\Factories\Factory;

class TiktokSourceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TiktokSource::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->userName,
            'fan_count' => $this->faker->numberBetween(100, 10000),
            'feed_id' => Feed::all()->random()->id,
        ];
    }
}
