<?php

namespace Database\Seeders;

use App\Models\Feed;
use App\Models\Post;
use Illuminate\Database\Seeder;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        Post::factory()
            ->count(500)
            ->create();

    }
}
