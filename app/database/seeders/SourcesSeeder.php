<?php

namespace Database\Seeders;

use App\Models\Feed;
use App\Models\InstagramSource;
use App\Models\TiktokSource;
use Illuminate\Database\Seeder;

class SourcesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        InstagramSource::factory()
            ->count(1000)
            ->create();

        TiktokSource::factory()
            ->count(1000)
            ->create();
    }
}
