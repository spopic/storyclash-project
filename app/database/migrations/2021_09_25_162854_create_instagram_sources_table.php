<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstagramSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_sources', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->bigInteger('fan_count');

            $table->unsignedBigInteger('feed_id');

            $table->foreign('feed_id')
                ->references('id')
                ->on('feeds');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagram_sources');
    }
}
