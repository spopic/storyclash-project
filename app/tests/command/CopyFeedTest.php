<?php

namespace Tests\Command;

use App\Console\Commands\CopyCommand;
use App\Models\Feed;
use App\Models\InstagramSource;
use App\Models\Post;
use App\Models\TiktokSource;
use Illuminate\Support\Facades\Artisan;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CopyFeedTest extends \TestCase
{

    public function __construct()
    {
        parent::__construct();

    }

    public function resetDatabase() {
        exec('php artisan migrate:fresh --database=mysql_test');
    }

    /**
     * Copy 1 Feed with both sources without posts
     *
     * @return void
     */

    public function testCopyCommand()
    {
        $this->resetDatabase();

        $this->assertCount(0, Feed::all());
        $this->assertCount(0, InstagramSource::all());
        $this->assertCount(0, TiktokSource::all());
        $this->assertCount(0, Post::all());

        Artisan::call(CopyCommand::class, [
            'feed-id' => 2
        ]);

        $this->assertCount(1, Feed::all());
        $this->assertNotEmpty(InstagramSource::all());
        $this->assertNotEmpty(TiktokSource::all());
        $this->assertEmpty(Post::all());
    }

    /**
     * Copy 1 Feed with Only Tiktok source
     *
     * @return void
     */

    public function testCopyCommandWithOnlyTiktok()
    {
        $this->resetDatabase();

        $this->assertCount(0, Feed::all());
        $this->assertCount(0, InstagramSource::all());
        $this->assertCount(0, TiktokSource::all());
        $this->assertCount(0, Post::all());

        Artisan::call(CopyCommand::class, [
            'feed-id' => 2,
            '--only' => 'tiktok'
        ]);

        $this->assertCount(1, Feed::all());
        $this->assertEmpty(InstagramSource::all());
        $this->assertNotEmpty(TiktokSource::all());
        $this->assertEmpty(Post::all());
    }

    /**
     * Copy 1 Feed with Only Instagram source
     *
     * @return void
     */

    public function testCopyCommandWithOnlyInstagram()
    {
        $this->resetDatabase();

        $this->assertCount(0, Feed::all());
        $this->assertCount(0, InstagramSource::all());
        $this->assertCount(0, TiktokSource::all());
        $this->assertCount(0, Post::all());

        Artisan::call(CopyCommand::class, [
            'feed-id' => 2,
            '--only' => 'instagram'
        ]);

        $this->assertCount(1, Feed::all());
        $this->assertNotEmpty(InstagramSource::all());
        $this->assertEmpty(TiktokSource::all());
        $this->assertEmpty(Post::all());
    }

    /**
     * Copy 1 Feed with Only Instagram source with 10 Posts
     *
     * @return void
     */

    public function testCopyCommandWithPostsAndInstagram()
    {
        $this->resetDatabase();

        $this->assertCount(0, Feed::all());
        $this->assertCount(0, InstagramSource::all());
        $this->assertCount(0, TiktokSource::all());
        $this->assertCount(0, Post::all());

        Artisan::call(CopyCommand::class, [
            'feed-id' => 2,
            '--only' => 'instagram',
            '--include-posts' => '10'
        ]);

        $this->assertCount(1, Feed::all());
        $this->assertNotEmpty(InstagramSource::all());
        $this->assertEmpty(TiktokSource::all());
        $this->assertCount(10, Post::all());
    }

    /**
     * Copy 1 Feed with Only Tiktok source with 15 Posts
     *
     * @return void
     */

    public function testCopyCommandWithPostsAndTiktok()
    {
        $this->resetDatabase();

        $this->assertCount(0, Feed::all());
        $this->assertCount(0, InstagramSource::all());
        $this->assertCount(0, TiktokSource::all());
        $this->assertCount(0, Post::all());

        Artisan::call(CopyCommand::class, [
            'feed-id' => 2,
            '--only' => 'tiktok',
            '--include-posts' => '15'
        ]);

        $this->assertCount(1, Feed::all());
        $this->assertEmpty(InstagramSource::all());
        $this->assertNotEmpty(TiktokSource::all());
        $this->assertCount(15, Post::all());
    }
}
