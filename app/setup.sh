docker exec -it storyclash_database mysql -uroot -ppassword -e "CREATE DATABASE docker;" && \
docker exec -it storyclash_database mysql -uroot -ppassword -e "CREATE DATABASE docker_prod;" && \
docker exec -it storyclash_database mysql -uroot -ppassword -e "CREATE DATABASE docker_test;" && \

docker exec -it storyclash_webserver php artisan migrate:fresh --database="mysql_dev"  && \
docker exec -it storyclash_webserver php artisan migrate:fresh --database="mysql_test"  && \
docker exec -it storyclash_webserver php artisan migrate:fresh --database="mysql_prod" --seed
