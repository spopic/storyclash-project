<?php

namespace App\Console\Commands;

use App\Models\Feed;
use App\Models\InstagramSource;
use App\Models\Post;
use App\Models\TiktokSource;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CopyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = 'feed:copy
                            {feed-id : The ID of the Feed}
                            {--only= : Optional choice for Instagram or Tiktok}
                            {--include-posts= : Count of posts to copy}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copy Feed from production database';

    protected $feedId;
    protected $newFeedId;
    protected $onlyOption;
    protected $postsCountOption;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        $this->setFeedId($this->argument('feed-id'));
        $this->setOnlyOption($this->option('only'));
        $this->setPostsCountOption($this->option('include-posts'));

        DB::beginTransaction();

        try {
            $this->copyFeed();
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e->getMessage());
        }

        DB::commit();


    }


    public function copyFeed()
    {
        $originalFeedData = Feed::on('mysql_prod')
            ->find($this->getFeedId());

        if (!$originalFeedData) {
            throw new \Exception('Entry not found');
        }
        $copiedFeed = Feed::create($originalFeedData->toArray());
        $this->setNewFeedId($copiedFeed->id);

        $this->copySources();
        $this->copyPosts();
    }

    public function copySources(): void
    {
        if (!in_array($this->getOnlyOption(), ['instagram', 'tiktok', null])) {
            throw new \Exception('Invalid option');
        }
        if ($this->getOnlyOption() === 'instagram' || !$this->getOnlyOption()) {
            $this->copy(InstagramSource::class);
        }

        if ($this->getOnlyOption() === 'tiktok' || !$this->getOnlyOption()) {
            $this->copy(TiktokSource::class);
        }
    }

    public function copyPosts(): void
    {
        if ($this->getPostsCountOption() > 0) {
            $this->copy(Post::class, $this->getPostsCountOption());
        }
    }

    public function copy($model, $limit = null)
    {
        $model::on('mysql_prod')
            ->where('feed_id', $this->getFeedId())
            ->limit($limit)
            ->get()
            ->each(function ($prodEntry) use ($model) {
                $devEntry = $prodEntry->toArray();
                $devEntry['feed_id'] = $this->getNewFeedId();

                $model::create($devEntry);
            });
    }


  /*
  |--------------------------------------------------------------------------
  | Getters and Setters
  |--------------------------------------------------------------------------
  */

    /**
     * @return mixed
     */
    public function getFeedId()
    {
        return $this->feedId;
    }

    /**
     * @param mixed $feedId
     */
    public function setFeedId($feedId): void
    {
        $this->feedId = $feedId;
    }

    /**
     * @return mixed
     */
    public function getPostsCountOption()
    {
        return $this->postsCountOption;
    }

    /**
     * @param mixed $postsCountOption
     */
    public function setPostsCountOption($postsCountOption): void
    {
        $this->postsCountOption = $postsCountOption;
    }

    /**
     * @return mixed
     */
    public function getOnlyOption()
    {
        return $this->onlyOption;
    }

    /**
     * @param mixed $onlyOption
     */
    public function setOnlyOption($onlyOption): void
    {
        $this->onlyOption = $onlyOption;
    }

    /**
     * @return mixed
     */
    public function getNewFeedId()
    {
        return $this->newFeedId;
    }

    /**
     * @param mixed $newFeedId
     */
    public function setNewFeedId($newFeedId): void
    {
        $this->newFeedId = $newFeedId;
    }

}