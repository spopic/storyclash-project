# Storyclash Demo Project

*Candidate: Stefan Popic*

## Description:

This project contains copy command with all required migrations. Databases are
generated with setup file. There are 3 databases in the project (Production, Dev and Test).
Naming convetions are not perfect for the databases so I will describe them here:

- docker = Development
- docker_prod = Production - This database is seeded and used for coping the data
- docker_test = Test Database - Test databse used only for unit tests


## Setup
From project root directory execute:

    docker-compose -f docker/docker-compose.yml up --build -d
    /bin/bash ./app/setup.sh

## Command

    php artisan feed:copy 

## Parameters:

| Option          | Description                     |
| -------------   | ------------------------------- |
| {feed-id}       | The ID of the Feed              |
| --only          | uses values instagram or tiktok |
| --include-posts | Count of posts to copy          |

## Examples                           

    php artisan feed:copy  1
    php artisan feed:copy --only=instagram 1
    php artisan feed:copy --only=instagram --include-posts=5 1


           

##Unit Test

 
    ./vendor/bin/phpunit --testsuite=CommandSuite

## Disclaimer

- I have used MacOS but there should be no problems setting up everything 
in Linux OS.

- All commands should be executed in the storyclash_webserver container or with 
prefix **docker exec -it storyclash_webserver** {command}
            